# Configuration settings for Nemo Clock

TIME_FORMAT = 'HMS' # Hours.Minutes.Seconds
DATE_FORMAT = 'MDY' # Month.Day.Year
TIME_ZONE = -6 # GMT is 0
