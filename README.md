# Nemo Clock

A simple clock for the [Micronote Atlas](https://micronote.tech/atlas/).  The Atlas uses an ESP8266 as its brain.  You will need the @Micronote [Atlas-Micropython libraries](https://gitlab.com/micronote/atlas-micropython) to run this.

Nemo Clock uses NTP to set its time, so you'll need to enter WIFI info into wifi_secrets.py.

Local settings can be saved in nemo_clock_config.py, including:
- Time Format (Not currently used.)
- Date Format ('MDY', 'DMY', 'YMD', or whatever combination you want.)
- Time Zone (Micropython normally runs on GMT, set whatever hours offset you want.)

Holding down the Mode button displays the date.

Pushing the Decr button uses NTP to reset the time.

-----

PS: When you assemble your Atlas, be sure to put the headers for the ESP8266 on the BOTTOM of the PCB.  I put the first one on top, installed the two 3-digit displays, and was about to put the second header on top when I noticed my problem.  I had to remove one of the displays.  That did something to it, so the B segment (the right-top) doesn't work on those three digits anymore.  


