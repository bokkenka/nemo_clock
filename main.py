# -*- coding: utf-8 -*-
#
# nemo_clock - A simple clock for the Micronote Atlas
# Copyright (c) 2020 Nemo Martinko
# https://gitlab.com/bokkenka/nemo_clock
#
# nemo_clock is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# nemo_clock is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from atlas import Atlas
import time
import ntptime
from wifi_secrets import *
from nemo_clock_config import *

class Clock(Atlas):
    '''  Create Clock class, importing from Atlas '''
    
    def __init__(self):
        super().__init__()
        self.tz = TIME_ZONE
    
    # Override button callbacks
    def mode_button_callback(self, pin):
        pass
    
    def incr_button_callback(self, pin):
        pass
    
    def decr_button_callback(self, pin):
        self.set_time_ntp()

    # Set time with NTP using timezone
    def set_time_ntp(self):
        self.write_num(000000)
        self.red_led.value(1)
        try:
            ntptime.settime()
            tm = time.localtime(time.mktime(time.localtime()) + int(self.tz*3600))
            tm = tm[0:3] + (0,) + tm[3:6] + (0,)
            self.rtc.datetime(tm)
            self.red_led.value(0)
        except:
            # Error setting time
            for _ in range(20):
                self.red_led.value(0)
                time.sleep_ms(100)
                self.red_led.value(1)
                time.sleep_ms(100)

    def show_time(self):
        # Show the time on the display
        t = time.localtime()
        tyr, tmon, tday, thr, tmin, tsec, twd, tyd = t
        num = thr * 10000 + tmin * 100 + tsec
        self.write_num(num, dp=0b010100)


    def show_date(self):
        # Show the Date on the display
        frmt = DATE_FORMAT
        num = 0
        t = time.localtime()
        tyr, tmon, tday, thr, tmin, tsec, twd, tyd = t
        tyr = tyr % 100
        tdict = {'Y': tyr, 'M': tmon, 'D': tday}
        num = tdict[frmt[0]] * 10000 + tdict[frmt[1]] * 100 + tdict[frmt[2]]
        self.write_num(num, dp=0b010100)


if __name__ == '__main__':
    device = Clock()
    device.connect_to_wifi(WIFI_ID, WIFI_PWD)
    device.set_time_ntp()

    while True:
        if device.mode_button.value() == 0:
            device.show_date()
        else:
            device.show_time()
        time.sleep_ms(50)

